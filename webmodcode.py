
# HTML module

class webpage:
    def __INIT__(self): #initializes header and body variables
        self.__header = ""                    # the HTML header
        self.__body = ""                      # the HTML content

    def breakk(self):  #function for adding breaks (extra empty lines) in the html file
        self.__bro="<br>"
        self.__brc="</br>"
        self.__br=self.__bro+self.__brc
        return self.__br

    def head(self): #function for header of the html file
        self.__stheader="FitClinic"      #text inside the title tags
        self.__headero="<head>"		#open header tag
        self.__headerc="</head>"  	#close header tag
        self.__titleo="<title>" 	#open title tag
        self.__titlec="</title>"	#close title tag
        self.__link='<link rel = "stylesheet" type = "text/css" href = "cssfile.css" />' #tag for importing the css file intot he html file(in order to format the table)   
        self.__header= self.__headero+self.__titleo+self.__stheader+self.__titlec+self.__link+self.__headerc #combining all elments of the header of the html file
        return self.__header
    
    def h1(self): #function for adding the heading in the html file
        self.__stheading="Fit Clinic Directory"  #text inside heading tag
        self.__divo='<div class="header">' #tag needed for styling the heading
        self.__divc="</div>"
        self.__h1o="<h1>"
        self.__h1c="</h1>"
        self.__h1=self.__divo+self.__h1o+self.__stheading+self.__h1c+self.__divc #combin gtags and string for the heading
        return self.__h1

    def hr(self): #function for styled (using css) line that seperated heading from the actual table
        self.__hr='<hr class="new1">'
        return self.__hr
        
    def tr1(self): #function returns first row of table1
        #strings for the first row of table 1
        self.__st1="Last Name"
        self.__st2="First Name"
        self.__st3="Gender"
        self.__st4="Age"
        self.__st5="Height(cm)"
        self.__st6="Weight(kg)"
        self.__st7="Last Service Purchased"
        self.__st8="Cost of Last Service Purchased"
        self.__tho="<th>"
        self.__thc="</th>"
        self.__tro="<tr>"
        self.__trc="</tr>"
        self.__th1=self.__tho+self.__st1+self.__thc
        self.__th2=self.__tho+self.__st2+self.__thc
        self.__th3=self.__tho+self.__st3+self.__thc
        self.__th4=self.__tho+self.__st4+self.__thc
        self.__th5=self.__tho+self.__st5+self.__thc
        self.__th6=self.__tho+self.__st6+self.__thc
        self.__th7=self.__tho+self.__st7+self.__thc
        self.__th8=self.__tho+self.__st8+self.__thc
        self.__tr1=self.__tro+self.__th1+self.__th2+self.__th3+ self.__th4+self.__th5+self.__th6+self.__th7+ self.__th8+self.__trc
        return self.__tr1
    
    def tr1rest (self, dataarray): #function returns rest of 200 rows for table 1
        self.__allrows=""
        for x in range(len(dataarray)): #loops get values of customer data from driver code 
            self.__tdo="<td>"		#and places values to create the 200 rows of customer data 
            self.__tdc="</td>"		#(with tags that will be returned as a string)
            self.__tho="<th>"
            self.__thc="</th>"
            self.__tro="<tr>"
            self.__trc="</tr>"

            #storing each customer's data in appropriate varibales every time the loop runs
            self.__lastname=dataarray[x].get_lastname()
            self.__firstname=dataarray[x].get_firstname()
            self.__gender=dataarray[x].get_gender()
            self.__age=str(dataarray[x].get_age())
            self.__height=str(dataarray[x].get_height())
            self.__weight=str(dataarray[x].get_weight())
            self.__purchase=str(dataarray[x].get_purchase())
            self.__cost="$"+str(dataarray[x].get_cost())

            #variable that combines data of customer and tags to create string for row of each customer 
	    #(makes row of customer 1 the fiorst tinme, makes row of customer 2 the second time and so on)
            self.__datarow1=self.__tro+self.__tdo+ self.__lastname+self.__tdc+self.__tdo+ self.__firstname+ self.__tdc+self.__tdo+ self.__gender+self.__tdc+self.__tdo+self.__age+ self.__tdc+self.__tdo+ self.__height+self.__tdc+self.__tdo+ self.__weight+ self.__tdc+self.__tdo+ self.__purchase+self.__tdc+self.__tdo+ self.__cost+ self.__tdc+self.__trc

            self.__allrows=self.__allrows+self.__datarow1 #adds the string for the row created of each customer, to the string of the row created for the previous customer creating a larger string with all the 200 rows

        return self.__allrows
                       
    def table1(self,tr1,allrows): #combinind table 1 tags, string for row 1 and string for rest 200 rows to create table 1
        self.__tableo='<table class="first">' #open table tag needed for the first table
        self.__tablec="</table>"
        self.__tableoo="<table>" #open table tag needed for the second table
        self.__table1= self.__tableo+self.__tr1+ self.__allrows+self.__tablec
        return self.__table1
                       
    def table2(self,revenue,avgpurchase,mostpopular): #table 2 containt the data for revenue, average purchase value and most popular service
        self.__tro="<tr>"
        self.__trc="</tr>"
        self.__tableo='<table class="first">' #open table tag needed for the first table
        self.__tablec="</table>"
        self.__tableoo="<table>" #open table tag needed for the second table
        self.__strevenue="Revenue:"
        self.__staveragepurchase="Average Purchase Value:"
        self.__stmostpopular="Most Popular Service:"

        self.__tr2=self.__tro+self.__tho+self.__strevenue+self.__thc+self.__tdo+"$"+str(revenue)+self.__tdc+self.__trc #combing tags and data for row1 containing revenue

        self.__tr3=self.__tro+self.__tho+self.__staveragepurchase+self.__thc+self.__tdo+"$"+str(avgpurchase)+self.__tdc+self.__trc #combing tags and data for row2 containing average purchase value

        self.__tr4=self.__tro+self.__tho+self.__stmostpopular+self.__thc+self.__tdo+mostpopular+self.__tdc+self.__trc #combing tags and data for row3 containing most popular service

        self.__table2=self.__tableoo+self.__tr2+self.__tr3+self.__tr4+self.__tablec #combinig all seperate rows for table 1
        return self.__table2
        
    def body(self,h1,br,hr,table1,table2): #function containing body tags and combining strings from heading, line, center/break tags, table 1 and table 2 to create string for whole body
        self.__bodyo="<body>"
        self.__bodyc="</body>"
        self.__centero="<center>"
        self.__centerc="</center>"

        self.__body=self.__bodyo+self.__h1+self.__br+self.__hr+self.__br+self.__br+ self.__centero+self.__table1+self.__br+self.__table2+self.__centerc+self.__br+self.__hr+self.__br+self.__bodyc

        self.__SGML = "<!DOCTYPE=html>" # the DTD for the document
        self.__html="<html>"            # html tag
        self.__htmlc="</html>"          # html close tag
        # The final assembly of the HTML file
        self.__doc = self.__SGML + self.__html + self.__header + self.__body + self.__htmlc
        return self.__doc
