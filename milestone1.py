################################variable list###############################

#variables used in the class
#last # variable used to return last name in the class
#first # variable used to return first name in class
#gender # varibale used to set default value and return gender of customer, in the class
#age # varibale used to set default value and return age of customer, in the class


#variables used while reading and storing values from data file
#fh # variable used to read and store data file
#lastname # variable where last name value from the data file is stored
#firstname # variable where last name value from the data file is stored
#gender # variable where last name value from the data file is stored
#age # variable where last name value from the data file is stored


c=[] #array to store objects contianing customer data

#variables used while intiating the main while loop for user interface
#choice #variable for choosing which function to perform with program
#num #varibale to enter number of customers to add/remove/search

#variable sused while addinf customers
#addlastname #varibale stores last name entered by user for adding to customer object list
#addfirstname #varibale stores first name entered by user for adding to customer object list
#addgender #varibale stores gender entered by user for adding to customer object list
#addage #varibale stores last name entered by user for adding to customer object list

#varibales used while removing customers
#removelastname #varibale stores last name entered by user for removing from customer object list
#removefirstname #varibale stores first name entered by user for removing from customer object list
#lastnames #storing last names from object array to match with user entered name in order to remove it

#variables used while searching for a customer
#searchlastname # last anme that is entered by the user and will be searched through the object list
#listoflastnames # varibale that stores last names in the original customer object array to compare with the ones entered by the user
#searchfirstname #first name entered by user that will be searched through the object list
#listoffirstnames # varibale that stores first names in the original customer object array to compare witht the ones entered by the user
#########################################################################################################################################


import string #importing library
from reusablecustomerobject import*

       
#reading data file for fit clininc
fh=open('fit_clinic_20.csv', 'r')
fh.seek(3) #making sure no weird charcters show up
for x in range(0,20):
    line=fh.readline() #reading each line in the data file one by one
    lastname, firstname, gender, age=line.split(",")  #splitting each customers data to store into object array
    c.append(clinic(lastname, firstname, gender, int(age))) #appending to array 'c' of objects that stores each customer's data

del fh #delting/closing dat file once it is used

#function to sort array of objects containing customer data, by last names
def sort (c):
    for y in range (len(c)): #using a buble sort
        for w in range (y+1, len(c)):
           if (c[w].get_lastname()< c[y].get_lastname()): #comapring the values of last names in  the object array
               temp= c[w] #storing the value temprarily in temp variable so it is not lost from the list
               c[w]= c[y]
               c[y]= temp

sort (c) #sorting the list by calling function

#printing title
print()
print("\t  o__ __o__/_    o     o           o__ __o     o     o                 o ")            

print("\t <|    v       _<|>_  <|>         /v     v\   <|>  _<|>_             _<|>_ ")          

print("\t < >                  < >        />       <\  / \                               ")      

print("\t  |              o     |       o/             \o/    o    \o__ __o     o        __o__ ")

print("\t  o__/_         <|>    o__/_  <|               |    <|>    |     |>   <|>      />  \  ")

print("\t  |             / \    |       \\              / \   / \   / \   / \   / \    o/      ")

print("\t <o>            \o/    |         \         /  \o/   \o/   \o/   \o/   \o/   <|       ")

print("\t  |              |     o          o       o    |     |     |     |     |     \\       ")

print("\t / \            / \    <\__       <\__ __/>   / \   / \   / \   / \   / \     _\o__</ ")


#introductory sentemce
print ("\n\n\tWelcome to the Fit clininc customer program that keeps track of all customers that attend the fit clinic!!")
print("\tThese are all the current customers registered with the clinic (sorted by last name): ")


#printing out sorted list of customer data
print("\n\n\t\t-------------------------------------------------")

print("\t\t|"+ "{:^14}".format("Last Name")+' |'+"{:^14}".format("Fisrt Name")+" |"+"{:^6}".format("Gender")+' |'+ "{:^6}".format("Age")+" | ")
print("\t\t-------------------------------------------------")


for z in range(len(c)):
   
    print("\t\t|"+ "{:<10}".format(c[z].get_lastname())+"\t|"+"{:<10}".format(c[z].get_firstname())+'\t|'+"{:<5}".format(c[z].get_gender())+'\t|'+ "{:<5}".format(c[z].get_age())+"\t| ")
    
print("\t\t-------------------------------------------------")
print()



while input("\n\tWould you like to futher explore or change the fit clininc program? ") in ('Y', 'y','yes','Yes','YES'):
    print("\n\tWhat would you like to do? ")
    print("\tAdd a customer to the program (A)")
    print("\tRemove a customer from the program (R)")
    print("\tSearch for a customer in the program (S)")

    choice=input("\n\tEnter A, R or S to make your choice: ")
    choice=choice.lower()
    if (choice not in ('a','r','s')):
        print("\tError! Enter a valid chocie!")
    
    while choice in ( 'a', 'r',  's'):
        if (choice=='a'):
            num=input("\n\n\tHow many customer would you like to add: ")
            for customers in range (int(num)):
                print ("\n\t\tEnter data for customer ",customers+1,": ")
                addlastname=input("\t\tLast Name: ")
                addlastname=addlastname.lower() #exception handling
                addlastname=string.capwords(addlastname)

                addfirstname=input("\t\tFirst Name: ")
                addfirstname= addfirstname.lower() #exception handling
                addfirstname=string.capwords(addfirstname)

                addgender=input("\t\tGender (f/m): ")
                addgender=addgender.lower() #exception hamdling


                addage=input("\t\tAge: ")
                c.append(clinic( addlastname,addfirstname,addgender,int(addage)))
                print ("\t\tCustomer has been added to to the Fit Clinic!!")
           
            sort (c) #sorting the list by calling function
            printingdata=input("\n\t\tWould you like to see the updated customer list? (y/n): ")
            print()
            printingdata=printingdata.lower()
            if (printingdata=='y' or printingdata=='yes'):
            #printing out sorted list of customer data
                print("\n\n\t ----------------------------------------------------")

                print("\t|"+ "{:^30}".format("Name")+' | '+"{:^10}".format("Gender")+' | '+ "{:^5}".format("Age")+"| ")
                print("\t ----------------------------------------------------")
    

                for z in range(len(c)):
   
                    print("\t|"+ "{:^30}".format(c[z].get_name())+' | '+"{:^10}".format(c[z].get_gender())+' | '+ "{:^5}".format(c[z].get_age())+"| ")

                print("\t ----------------------------------------------------")
                break
            else:
                break

           
            break


   
        if (choice=='R' or choice=='r'):
            num=input("\n\n\tHow many customer would you like to remove: ")
            
            for customers in range (int(num)):
                print ("\n\tEnter data for customer ",customers+1,": ")
                removelastname=input("\t\tLast Name: ")
                removelastname=removelastname.lower() #exception handling
                removelastname=string.capwords(removelastname)
                removefirstname=input("\t\tFirst Name: ")
                found=False
                for i in range(len(c)):
                    lastnames=c[i].get_lastname()
                    if lastnames==removelastname:
                        found=True
                        del c[i]
                        c[i].destructor()
                        break
                    
                sort (c) #sorting the list by calling function
                if found==True:
                    printingdata=input("\n\t\tWould you like to see the updated customer list? (y/n): ")
                    print()
                    printingdata=printingdata.lower()
                    if (printingdata=='y' or printingdata=='yes'):
                    #printing out sorted list of customer data
                        print("\n\n\t ----------------------------------------------------")

                        print("\t|"+ "{:^30}".format("Name")+' | '+"{:^10}".format("Gender")+' | '+ "{:^5}".format("Age")+"| ")
                        print("\t ----------------------------------------------------")
    

                        for z in range(len(c)):
   
                            print("\t|"+ "{:^30}".format(c[z].get_name())+' | '+"{:^10}".format(c[z].get_gender())+' | '+ "{:^5}".format(c[z].get_age())+"| ")

                        print("\t ----------------------------------------------------")
                        break
                    else:
                        break
            
                if found==False:
                    print("\n\t\tError!Customer not found")
                
            break



        if (choice=='S' or choice=='s'):
            choose=input("\n\tSearch by Last name or First name (L/F)? ")

            if (choose=='L' or choose=='l'):
                num=input("\n\n\tHow many customer would you like to search: ")
                for customers in range (int(num)):
                    print ("\tEnter data for customer ",customers+1,": ")
                    searchlastname=input("\t\tLast Name: ")
                    searchlastname=searchlastname.lower() #exception handling
                    searchlastname=string.capwords(searchlastname)
                    found=False
                    for i in range(len(c)):
                        listoflastnames=c[i].get_lastname()
                        if listoflastnames==searchlastname:
                            found=True
                            #printing out customer data
                            print("\n\t ----------------------------------------------------")
    
                            print("\t|"+ "{:^30}".format("Name")+' | '+"{:^10}".format("Gender")+' | '+ "{:^5}".format("Age")+"| ")
                            print("\t ----------------------------------------------------")
                            print("\t|"+ "{:^30}".format(c[i].get_name())+' | '+"{:^10}".format(c[i].get_gender())+' | '+ "{:^5}".format(c[i].get_age())+"| ")

                            print("\t ----------------------------------------------------")
                            break
                    if found==False:
                        print ("\n\t\tError! Customer not found")
                        break
            

                               
            if (choose=='F' or choose=='f'):
                 num=input("\n\tHow many customer would you like to search: ")
                 for customers in range (int(num)):
                     print ("\tEnter data for customer ",customers+1,": ")
                     searchfirstname=input("\t\tFirst Name: ")
                     searchfirstname=searchfirstname.lower() #exception handling
                     searchfirstname=string.capwords(searchfirstname)
                     found=False
                     for i in range(len(c)):
                         listoffirstnames=c[i].get_firstname()
                         if listoffirstnames==searchfirstname:
                             found=True
                             #printing out customer data
                             print("\n\t ----------------------------------------------------")
    
                             print("\t|"+ "{:^30}".format("Name")+' | '+"{:^10}".format("Gender")+' | '+ "{:^5}".format("Age")+"| ")
                             print("\t ----------------------------------------------------")
                             print("\t|"+ "{:^30}".format(c[i].get_name())+' | '+"{:^10}".format(c[i].get_gender())+' | '+ "{:^5}".format(c[i].get_age())+"| ")

                             print("\t ----------------------------------------------------")
                             break
                     if found==False:
                         print ("\n\tError! Customer not found")
                         break
                
            
            break
               
                 
                           
print("\n\n\tThe program is closing now...See you later!")                

                 
