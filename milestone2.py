###############################variable list##############################

#variables used in the class
#last # variable used to return last name in the class
#first # variable used to return first name in class
#gender # varibale used to set default value and return gender of customer, in the class
#age # varibale used to set default value and return age of customer, in the class


#variables used while reading and storing values from data file
#fh # variable used to read and store data file
#lastname # variable where last name value from the data file is stored
#firstname # variable where last name value from the data file is stored
#gender # variable where last name value from the data file is stored
#age # variable where last name value from the data file is stored


c=[] #array to store objects contianing customer data


#liberaries imported and used

import string #importing library
import statistics
from statistics import mode
from reusablecustomerobject import*
import math

########################################################################################################################################

       

#reading data file for fit clininc
fh=open('fit_clinic_200.csv', 'r')
fh.seek(3) #making sure no weird charcters show up
for x in range(0,200):
    line=fh.readline() #reading each line in the data file one by one
    lastname, firstname, gender, age, height, weight, lastservice, cost=line.split(",")  #splitting each customers data to store into object array
    c.append(clinic(lastname, firstname, gender, int(age))) #appending to array 'c' of objects that stores each customer's data
    cost=cost.strip("$")
    cost=cost.strip("\n")
    c[x].set_height(height)
    c[x].set_weight(weight)
    c[x].set_purchase(lastservice)
    c[x].set_cost(float(cost))
    
   

del fh #delting/closing dat file once it is used

#function to sort array of objects containing customer data, by last names
def sort (c):
    for y in range (len(c)): #using a buble sort
        for w in range (y+1, len(c)):
           if (c[w].get_lastname()< c[y].get_lastname()): #comapring the values of last names in the object array
               temp= c[w] #storing the value temprarily in temp variable so it is not lost from the list
               c[w]= c[y]
               c[y]= temp
               


sort (c) #sorting the list by calling function

#printing title
print()
print("\t  o__ __o__/_    o     o           o__ __o     o     o                 o ")            

print("\t <|    v       _<|>_  <|>         /v     v\   <|>  _<|>_             _<|>_ ")          

print("\t < >                  < >        />       <\  / \                               ")      

print("\t  |              o     |       o/             \o/    o    \o__ __o     o        __o__ ")

print("\t  o__/_         <|>    o__/_  <|               |    <|>    |     |>   <|>      />  \  ")

print("\t  |             / \    |       \\              / \   / \   / \   / \   / \    o/      ")

print("\t <o>            \o/    |         \         /  \o/   \o/   \o/   \o/   \o/   <|       ")

print("\t  |              |     o          o       o    |     |     |     |     |     \\       ")

print("\t / \            / \    <\__       <\__ __/>   / \   / \   / \   / \   / \     _\o__</ ")


#introductory sentemce
print ("\n\n\tWelcome to the Fit clininc customer program that keeps track of all customers that attend the fit clinic!!")
print("\tThese are all the current customers registered with the clinic (sorted by last name): ")
                 

#calcuting revenue

rev=0

for s in range (len(c)):
    rev=rev+c[s].get_cost()

averagepurchasevalue= rev/len(c)

counter1=0
counter2=0
counter3=0


for x in range(len(c)):
    if (c[x].get_purchase()== "Massage"):
        counter1=counter1+1
    if (c[x].get_purchase()== "Chiropractic"):
        counter2=counter2+1
    if (c[x].get_purchase()== "Acupuncture"):
        counter3=counter3+1

mostcommon=" "
if (counter1>counter2) &(counter1>counter3):
    mostcommon="Massage"
if(counter2>counter1) &(counter2>counter3):
    mostcommon="Chiropractic"
if (counter3>counter1)& (counter3>counter2):
    mostcommon="Acupuncture"
if (counter1==counter2) & (counter1==counter3) & (counter2==counter3):
    mostcommon="blah"
  





print(mostcommon)    
 
print(rev)
print(averagepurchasevalue)

print("These are all the current customers registered with the clinic (sorted by last name): ")

#printing out sorted list of customer data
print("\n\n\t -----------------------------------------------------------------------------------------------------------------------------------------")

print("\t|"+ "{:^14}".format("Last Name")+' |'+"{:^14}".format("Fisrt Name")+" |"+"{:^7}".format("Gender")+'|'
      + "{:^6}".format("Age")+" | "+ "{:^13}".format("Height(cm)")+' | '+"{:^13}".format("Weight(kg)")+" | "
      +"{:^22}".format("Last Service Purchased")+' | '+ "{:^30}".format("Cost of Last Service Purchased")+" | ")


print("\t -----------------------------------------------------------------------------------------------------------------------------------------")


for z in range(len(c)):
   
    print("\t| "+ "{:<10}".format(c[z].get_lastname())+"\t| "+"{:<10}".format(c[z].get_firstname())+'\t| '+"{:<5}".format(c[z].get_gender())+'\t| '+ "{:<5}".format(c[z].get_age())+"\t| "
          + "{:<7}".format(c[z].get_height())+"\t| " + "{:<7}".format(c[z].get_weight())+"\t| " + "{:<20}".format(c[z].get_purchase())+"\t | $" + "{:<25}".format(c[z].get_cost())+"\t  |")
    
print("\t -----------------------------------------------------------------------------------------------------------------------------------------")
print("\t| Revenue: $"+ "{:<10}".format(rev),"\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t  |")
print("\t -----------------------------------------------------------------------------------------------------------------------------------------")
print("\t| Average Purchase Value: $"+"{:<10}".format(averagepurchasevalue),"\t\t\t\t\t\t\t\t\t\t\t\t\t  |")
print("\t -----------------------------------------------------------------------------------------------------------------------------------------")
print('\t| Most Popular Service: '+"{:<5}".format(mostcommon),"\t\t\t\t\t\t\t\t\t\t\t\t\t  |")
print("\t -----------------------------------------------------------------------------------------------------------------------------------------")
print()



                           
print("\n\n\tThe program is closing now...See you later!")                






  
