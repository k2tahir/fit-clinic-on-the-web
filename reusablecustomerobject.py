#class for the clininc
class clinic:
    def __init__(self, lastname,firstname, gender, age):
        self.__last=lastname #assigning variable defaut value for last name
        self.__first=firstname #assigning variable defaut value for first name
        self.__gender=gender
        self.__age=int(age)
        self.__cost=0.00

    def __del__(self):
        pass
    def destructor(self):
        print("Customer is removed")
    def get_lastname(self): #returning just last name
        return self.__last
    def get_firstname(self): #returning just first name
        return self.__first
    def get_name(self):
        return self.__first+" "+ self.__last #returning first and last name together with space inbetween
    def get_gender(self):
        return self.__gender
    def get_age(self):
        return self.__age
    def set_height(self,height):
        self.__height=float(height)
    def get_height(self):
        return self.__height
    def set_weight(self, weight):
        self.__weight=float(weight)
    def get_weight(self):
        return self.__weight
    def set_purchase(self, purchase):
        self.__purchase=str(purchase)
    def get_purchase(self):
        return self.__purchase
    def set_cost(self, cost):
        self.__cost=cost
    def get_cost(self):
        return self.__cost
